# -*- coding: utf-8 -*-#
# -------------------------------------------------------------------------------
# Name:         main
# Description:  
# Date:         2020/5/21
# -----------------------------------------------------------------------------
import paramiko
import argparse
import psycopg2
import csv

pgHost = '172.16.2.53'
pgPort = 5432
pgDB = 'economicanalysis'
pgUSER = 'economic'
pgPW = 'economic@ynty1'
host = 'ftp.unionpaysmart.com'
post = 8130
user = 'tengyun'
pw = '75G40w0NizEn'


def testFTP():
    transport = paramiko.Transport((host, post))    # 获取Transport实例
    transport.connect(username=user, password=pw)  # 建立连接
    # 建立sftp对象
    sftp = paramiko.SFTPClient.from_transport(transport)
    print(sftp.listdir('/to_tengyun/月报/'))
    # sftp.get('/to_tengyun/月报/yunnan_monthly_txt1_202001.csv',
    #                   'E:/0.git_project/auto_get_unionpay/data/yunnan_monthly_txt1_202001.csv')
    transport.close()
    pass



def proMonth_01(conn, _date):
    '''
    月报txt1 : yunnan_monthly_txt1_202001.csv
    '''
    _date = ''.join(_date.split('-')[:-1])
    fileName = 'yunnan_monthly_txt1_{date}.csv'.format(date=_date)
    remotePath = '/to_tengyun/月报/'+fileName
    localPath = './data/'+fileName
    # sftpGet(remotePath, localPath)
    data = readCSV(localPath)
    for item in data:
        _proMonth_01(conn, item)
    print('')


def _proMonth_01(conn, item):
    # 0:交易月份, 1:云南省, 2:云南省各州市, 3:云南省各区县, 4:客源省份, 5:客源城市, 6:消费性别, 7:消费年龄, 8:6大消费大类,
    # 9:消费金额（元）, 10:消费卡数（张）
    trade_date = item[0]
    if '-' not in trade_date:   # 如果不是时间字符串
        return

    trade_date += '-01'     # 2020-01 -> 2020-01-01
    trade_count = int(float(item[10]))
    valueList = (trade_date, item[3], item[2], item[4], item[5], item[6], item[7], item[8], item[9], trade_count)
    sql = 'INSERT INTO yunnan_union_pay_cross_attr_analysis (trade_date, yn_district, belongarea, src_province,' \
          ' src_city, gender, age, category, trade_amount, trade_count) VALUES ( %s,%s,%s,%s,%s,%s,%s,%s,%s,%s )' \
          ' ON CONFLICT DO NOTHING;;'
    cur = conn.cursor()
    cur.execute(sql, valueList)
    conn.commit()
    print('??')
    pass


def sftpGet(remotePath, localPath):
    transport = paramiko.Transport((host, post))  # 获取Transport实例
    transport.connect(username=user, password=pw)  # 建立连接
    # 建立sftp对象
    sftp = paramiko.SFTPClient.from_transport(transport)
    sftp.get(remotePath, localPath)
    transport.close()
    return


def readCSV(localPath):
    data = []
    with open(localPath, 'r', encoding='utf-8') as fb:
        lines = csv.reader(fb)
        for line in lines:
            data.append(line)
    return data


def handle(conn, _date, _type):
    if _type == 'month-01':
        proMonth_01(conn, _date)
    pass


def initPgDB(dbName, user, passwd, host, port):
    conn = psycopg2.connect(database=dbName,
                            user=user,
                            password=passwd,
                            host=host,
                            port=port,
                            connect_timeout=3)
    # cur = conn.cursor()
    return conn


def parse_ages():
    parser = argparse.ArgumentParser(description="自动同步银联数据")
    parser.add_argument('--date', default='', help=' date : [yyyy-mm-dd]', type=str)
    parser.add_argument('--type', default='', help=' type : [month-01] or [days-01]')
    return parser.parse_args()


def main():
    # testFTP()
    conn = initPgDB(pgDB, pgUSER, pgPW, pgHost, pgPort)
    cur = conn.cursor()
    cur.execute("SET TIME ZONE 'Asia/Chongqing';")
    conn.commit()
    ages = parse_ages()
    if ages.date == '' or ages.type == '':
        print('请指定要同步的日,月份或类型')
        return
    handle(conn, ages.date, ages.type)


if __name__ == '__main__':
    main()
